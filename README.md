# s3
[![pipeline status](https://gitlab.com/duduksini/s3/badges/master/pipeline.svg)](https://gitlab.com/duduksini/s3/commits/master)  

duduksini's main s3 bucket to store lambda artifacts, database backups, static files, etc.

## Deployment to AWS
Gitlab CI deploys latest master branch automatically to AWS via CloudFormation template in **/aws**.

## Deployment to AWS from local machine
1. Install [awscli](https://docs.aws.amazon.com/cli/latest/userguide/installing.html), minimum version 1.14.58
1. set `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_DEFAULT_REGION`
2. In **/aws**, run `deploy.sh`
